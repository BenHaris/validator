#include <stdio.h>
#include <stdlib.h>
#include "checks.h"

void checkInput(int argc, char *argv[]){
    if(argc > 3 ){
        printf("TOO MANY ARGUMNENTS");
        help();
        exit(EXIT_FAILURE);
    }
    switch(argc){

    case 3:
        if(!argv[0] || !argv[1] || !argv[2]){
            help();
            exit(EXIT_FAILURE);
        }
        break;
    case 2:
        if(!argv[0] || !argv[1]){
            help();
            exit(EXIT_FAILURE);
        }
        break;
    case 1:
        printf("NO COMMAND FOUND\n");
        help();
        exit(EXIT_FAILURE);
    }
}

void checkCommand(char *command, char *tocheck){

    if((!command || command[0] != '-') || !tocheck){
        help();
        exit(EXIT_FAILURE);
    }
    char com = command[1];

    switch(com){

        case 's':
            checkedValue(checkSingle(tocheck));
            break;
        case 'b':
            checkedValue(bulkOp(tocheck));
            break;
        default:
            help();
            break;
    }
}
