#ifndef COMMANDS_H_INCLUDED
#define COMMANDS_H_INCLUDED

void checkInput(int argc, char *argv[]);

void checkCommand(char *command, char *toCheck);

#endif // COMMANDS_H_INCLUDED
