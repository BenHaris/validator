#ifndef CHECKS_H_INCLUDED
#define CHECKS_H_INCLUDED

void help();

int checkSingle(char *tocheck);

void checkedValue(int value);

int bulkOp(char *tocheck);

#endif // CHECKS_H_INCLUDED
