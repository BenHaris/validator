#include <stdio.h>
#include <stdlib.h>

#define MAX_LEN 100

void help(){

    printf("\n***Email Validator***\nUsage: <command> <checkable>\nAvailable commands:"
           "\n-s  (Single check)\n-b  (Bulk operation)\n");
}

int checkSingle(char *tocheck){

    if(!tocheck){
        return 0;
    }
    int i = 0;
    int first = 0, last = 0;
    int dots = 0, ats = 0;
    //Measure length
    while(tocheck[i] != '\0'){

        if(tocheck[i] == '@'){
            ats++;
        }else if(tocheck[i] == '.'){
            dots++;
        }
        i++;
    }
    last = i-1;

    //First and last char check
    if(tocheck[first] == '.' || tocheck[last] == '.' || tocheck[first] == '@' || tocheck[last] == '@'){
        return 0;
    }
    //Only 1 '@' and At Least 1 '.'
    if(ats != 1  || dots < 1){
        return 0;
    }
    //Check values next to '@'s and '.'s
    i = 0;
    while(tocheck[i] != '\0'){

        switch(tocheck[i]){
            case '@':
                if(tocheck[i-1] == '@' || tocheck[i-1] == '.'){
                    return 0;
                }else if(tocheck[i+1] == '@' || tocheck[i+1] == '.'){
                    return 0;
                }
                break;
            case '.':
                if(tocheck[i-1] == '@' || tocheck[i-1] == '.'){
                    return 0;
                }else if(tocheck[i+1] == '@' || tocheck[i+1] == '.'){
                    return 0;
                }
        }
        i++;
    }
    return 1;
}

void checkedValue(int value){
    printf("***Validation completed, %d correct value(s) found***\n", value);
}

int bulkOp(char *tocheck){

    if(!tocheck){
        return 0;
    }
    FILE *f = fopen(tocheck, "r");
    if(!f){
        printf("No file named %s found\n", tocheck);
        return 0;
    }
    char c, *strings = (char*)malloc(MAX_LEN*sizeof (char));
    int i = 0, correct = 0;
    do{
        c = getc(f);
        strings[i] = c;
        i++;
        if(c == ' ' || c == '\n' || c == -1){
            strings[i-1] = '\0';
            if(checkSingle(strings)){
                printf("%s\n", strings);
                correct++;
            }
            i = 0;
        }
    }
    while(c != EOF);
    fclose(f);
    free(strings);
    return correct;
}
