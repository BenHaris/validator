#include <stdio.h>
#include <stdlib.h>
#include "commands.h"

int main(int argc, char *argv[])
{
    checkInput(argc, argv);
    checkCommand(argv[1], argv[2]);

    return 0;
}
